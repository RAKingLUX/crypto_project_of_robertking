#JSON Exercise

[
  {
      "country" : "USA",
      "city" : "Miami,FL",
      "weather" : "Sunny",
      "chance of rain" : False,
      "temperature" : "30c",
      "wind speed" : "5kmph",
      
  },
  {
      "country" : "England",
      "city" : "London,UK",
      "chance of rain" : True,
      "weather" : "Rain",
      "temperature" : "15c",
      "wind speed" : "15kmph",
          
  },
  
]