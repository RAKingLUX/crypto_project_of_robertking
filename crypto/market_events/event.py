from abc import ABC, abstractmethod

class Event(ABC):
    def __init__(self, senders):
        self.senders = senders
    
    @property 
    @abstractmethod
    def condition(self):
        pass
   
    @abstractmethod
    def message(self):
        pass
    
    @abstractmethod
    def check(self):
        if self.condition:
            for sender in self.senders:
                sender.send(self.message)
    
class MockEvent(Event):
    @property
    def condition(self):
        return True
    
    @property
    def check(self):
        message = "Now you have more money than 5 minutes ago!"
        return message