from crypto.app import App
from crypto.market_events.event import MockEvent
from crypto.message_delivery.sender import MockSender

senders = [MockSender()]
events = [MockEvent(senders)]

app = App(events, 5)
app.start()
